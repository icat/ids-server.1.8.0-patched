# Patched

It allows to make the zip file even if the files are missing.
It generates a report listing the missing files:

```
# Archiving Report
# Restoration completed successfully with warnings
	[WARNING]	Missing file	/data/id30a1/inhouse/opid30a1/20181126/RAW_DATA/AFAMIN/AFAMIN-CD024584_B04-3_2/MXPressA_01/opid30a1-line-AFAMIN-CD024584_B04-3_2-line-AFAMIN-CD024584_B04-3_2_3_2450915.h5
	[WARNING]	Missing file	/data/id30a1/inhouse/opid30a1/20181126/RAW_DATA/AFAMIN/AFAMIN-CD024584_B04-3_2/MXPressA_01/opid30a1-id30a1-line-AFAMIN-CD024584_B04-3_2_3_2450915.h5

```

# Configuration

## run.properties
```
# General properties
icat.url = http://ovm-icat2.esrf.fr:8080


plugin.zipMapper.class = org.icatproject.ids.storage.esrf.ZipMapper

# Properties for main storage
plugin.main.class = org.icatproject.ids.storage.esrf.MainFileStorage
plugin.main.dir = /data/icat/main
plugin.main.icat.url = https://icat.esrf.fr
plugin.main.icat.user = admin
plugin.main.icat.password = *****
plugin.main.icat.auth = db
plugin.main.restoration.pattern=/data/visitor/{investigation}/{instrument}/restored/{sample}_{dataset}

cache.dir = /data/icat/cache

# Ids server settings
preparedCount = 10000
processQueueIntervalSeconds = 5
rootUserNames = root
sizeCheckIntervalSeconds = 60
reader = db username admin password *****
!readOnly = true
maxIdsInQuery = 1000

# Properties for archive storage
plugin.archive.class = org.icatproject.ids.storage.esrf.ArchiveFileStorage
plugin.archive.dir = /ilm/restored
plugin.archive.queue_out_tmp = /ilm/outq/tmp
plugin.archive.queue_out_wait = /data/ilm/outq/wait
plugin.archive.restoration_time_out = 86400
plugin.archive.restorationFolderPattern={sample}_{dataset}

writeDelaySeconds = 60
startArchivingLevel1024bytes = 5000000
stopArchivingLevel1024bytes =  4000000
storageUnit = dataset
tidyBlockSize = 500

# File checking properties
filesCheck.parallelCount = 0
filesCheck.gapSeconds = 5
filesCheck.lastIdFile = /tmp/ids/lastIdFile
filesCheck.errorLog = /tmp/ids/errorLog

# Link properties
linkLifetimeSeconds = 3600

# JMS Logging
log.list = READ WRITE INFO LINK MIGRATE PREPARE

# JMS - uncomment and edit if needed
!jms.topicConnectionFactory = java:comp/DefaultJMSConnectionFactory
```
